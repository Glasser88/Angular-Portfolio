(function(){
  'use strict';
    angular
    .module('glasser')
    .controller('MainController', function($scope, $mdSidenav, $timeout, $mdUtil, $log, $mdDialog) {

      // Portfolio Dialogs //
      $scope.status = '  ';

      $scope.quakeDialog = function(ev) {
        $mdDialog.show(
        $mdDialog.alert()
       .parent(angular.element(document.querySelector('#popupContainer')))
       .clickOutsideToClose(true)
       .title('')
       .content('<div class="card"><div class="cardTop"><div class="portfolio-image"><div class="portfolio-specs earthquake"><div class="card-inside-container"><div class="card-inside-bg"></div><div class="card-inside"></div></div></div></div></div><div class="cardBottom"><h2 class="cardName">Earthquake Tracker</h2><h6 class="cardNickname">Angular.js</h6><h6 class="cardDescription">Earthquake tracker allows anyone to track earthquakes with real time data constantly updating the map. You can see the earthquakes that have happened in the month, week, day, or hour.</h6></div></div>')
       .ariaLabel('Earthquake Tracker')
       .ok('Close')
       .targetEvent(ev)
   );
 };

      $scope.halibutDialog = function(ev) {
        $mdDialog.show(
        $mdDialog.alert()
       .parent(angular.element(document.querySelector('#popupContainer')))
       .clickOutsideToClose(true)
       .title('')
       .content('<div class="card"><div class="cardTop"><div class="portfolio-image"><div class="portfolio-specs halibut"><div class="card-inside-container"><div class="card-inside-bg"></div><div class="card-inside"></div></div></div></div></div><div class="cardBottom"><h2 class="cardName">Just For The Halibut</h2><h6 class="cardNickname">Angular.js</h6><h6 class="cardDescription">Just For The Halibut is an application designed for the sporting fisherman. It bring in real time weather data and predictions as well as recipe suggestions to further enhance your fishing experience!</h6></div></div>')
       .ariaLabel('Just For The Halibut')
       .ok('Close')
       .targetEvent(ev)
   );
 };

       $scope.smallGameDialog = function(ev) {
         $mdDialog.show(
         $mdDialog.alert()
        .parent(angular.element(document.querySelector('#popupContainer')))
        .clickOutsideToClose(true)
        .title('')
        .content('<div class="card"><div class="cardTop"><div class="portfolio-image"><div class="portfolio-specs small-game-hunter"><div class="card-inside-container"><div class="card-inside-bg"></div><div class="card-inside"></div></div></div></div></div><div class="cardBottom"><h2 class="cardName">Small Game Hunter</h2><h6 class="cardNickname">jQuery</h6><h6 class="cardDescription">Small Game Hunter is a first person shooter arcade style game. It takes you deep into the jungle to hunt for the rare and exotic amazonian duck. But be careful not to shoot the friendly Giraffee!</h6></div></div>')
        .ariaLabel('Small Game Hunter')
        .ok('Close')
        .targetEvent(ev)
    );
  };

      $scope.tarpDialog = function(ev) {
        $mdDialog.show(
        $mdDialog.alert()
       .parent(angular.element(document.querySelector('#popupContainer')))
       .clickOutsideToClose(true)
       .title('')
       .content('<div class="card"><div class="cardTop"><div class="portfolio-image"><div class="portfolio-specs surfboard"><div class="card-inside-container"><div class="card-inside-bg"></div><div class="card-inside"></div></div></div></div></div><div class="cardBottom"><h2 class="cardName">Tarp Clothing</h2><h6 class="cardNickname">Angular.js</h6><h6 class="cardDescription">Tarp Clothing is a brand dedicated to quality and design. You will not want to wear anything else once you have tried one of these great shirts on. It is the one and only tarp you will ever need!</h6></div></div>')
       .ariaLabel('Tarp Clothing')
       .ok('Close')
       .targetEvent(ev)
   );
 };
})


.directive('scroll', function ($window) {

  return function(scope, element, attrs) {

    /* header DOM element with md-page-header attribute */
    var header         = document.querySelector('[md-page-header]');
    /* Store header dimensions to initialize header styling */
    var baseDimensions = header.getBoundingClientRect();
    /* DOM element with md-header-title attribute (title in toolbar) */
    var title          = angular.element(document.querySelector('[md-header-title]'));
    /* DOM element with md-header-picture attribute (picture in header) */
    var picture        = angular.element(document.querySelector('[md-header-picture]'));
    /* DOM element with main-fab class (a DOM element which contains the main float action button element) */
    var fab            = angular.element(document.querySelector('.main-fab'));
    /* The height of a toolbar by default in Angular Material */
    var legacyToolbarH = 64;
    /* The mid-height of a float action button by default in Angular Material */
    var legacyFabMid   = 56/2;
    /* The zoom scale of the toolbar title when it's placed at the bottom of the header picture */
    var titleZoom      = 1.5;
    /* The primary color palette used by Angular Material */
    var primaryColor   = [33,33,33];

    function styleInit () {
      title.css('padding-left','16px');
      title.css('position','relative');
      title.css('transform-origin', '24px');
    }

    function handleStyle(dim) {
      fab.css('top',(dim.height-legacyFabMid)+'px');
      if ((dim.bottom-baseDimensions.top) > legacyToolbarH) {
        title.css('top', ((dim.bottom-baseDimensions.top)-legacyToolbarH)+'px');
        element.css('height', (dim.bottom-baseDimensions.top)+'px');
        title.css('transform','scale('+((titleZoom-1)*ratio(dim)+1)+','+((titleZoom-1)*ratio(dim)+1)+')');

      } else {
        title.css('top', '0px');
        element.css('height', legacyToolbarH+'px');
        title.css('transform','scale(1,1)');
      }
      if ((dim.bottom-baseDimensions.top) < legacyToolbarH*2 && !fab.hasClass('hide')) {
        fab.addClass('hide');
      }
      if((dim.bottom-baseDimensions.top)>legacyToolbarH*2 && fab.hasClass('hide')) {
        fab.removeClass('hide');
      }
      element.css('background-color','rgba('+primaryColor[0]+','+primaryColor[1]+','+primaryColor[2]+','+(1-ratio(dim))+')');
      picture.css('background-position','50% '+(ratio(dim)*50)+'%');
      /* Uncomment the line below if you want shadow inside picture (low performance) */
      //element.css('box-shadow', '0 -'+(dim.height*3/4)+'px '+(dim.height/2)+'px -'+(dim.height/2)+'px rgba(0,0,0,'+ratio(dim)+') inset');
    }

    function ratio(dim) {
      var r = (dim.bottom-baseDimensions.top)/dim.height;
      if(r<0) return 0;
      if(r>1) return 1;
      return Number(r.toString().match(/^\d+(?:\.\d{0,2})?/));
    }

    styleInit();
    handleStyle(baseDimensions);

      /* Scroll event listener */
    angular.element($window).bind('scroll', function() {
      var dimensions = header.getBoundingClientRect();
      handleStyle(dimensions);
      scope.$apply();
    });

    /* Resize event listener */
    angular.element($window).bind('resize',function () {
      baseDimensions = header.getBoundingClientRect();
      var dimensions = header.getBoundingClientRect();
      handleStyle(dimensions);
      scope.$apply();
    });

  };

})





  .controller('SkillsController', function ($scope) {
  $scope.labels = ['Angular Material', 'Google APIs', 'AngularJS'];
  $scope.data = [33, 33, 33];
  })

  .controller('AppCtrl', function ($scope, $timeout, $mdSidenav, $log) {
    $scope.toggleLeft = buildDelayedToggler('left');
    $scope.toggleRight = buildToggler('right');
    $scope.isOpenRight = function(){
      return $mdSidenav('right').isOpen();
    };
    /**
     * Supplies a function that will continue to operate until the
     * time is up.
     */
    function debounce(func, wait, context) {
      var timer;
      return function debounced() {
        var context = $scope,
            args = Array.prototype.slice.call(arguments);
        $timeout.cancel(timer);
        timer = $timeout(function() {
          timer = undefined;
          func.apply(context, args);
        }, wait || 10);
      };
    }
    /**
     * Build handler to open/close a SideNav; when animation finishes
     * report completion in console
     */
    function buildDelayedToggler(navID) {
      return debounce(function() {
        $mdSidenav(navID)
          .toggle()
          .then(function () {
            $log.debug('toggle ' + navID + ' is done');
          });
      }, 200);
    }
    function buildToggler(navID) {
      return function() {
        $mdSidenav(navID)
          .toggle()
          .then(function () {
            $log.debug('toggle ' + navID + ' is done');
          });
      };
    }
 })


  .controller('LeftCtrl', function ($scope, $timeout, $mdSidenav, $log) {
    $scope.close = function () {
      $mdSidenav('left').close()
        .then(function () {
          $log.debug('close LEFT is done');
        });
    };
  })

  .controller('RightCtrl', function ($scope, $timeout, $mdSidenav, $log) {
    $scope.close = function () {
      $mdSidenav('right').close()
        .then(function () {
          $log.debug('close RIGHT is done');
        });
    };
  })

.controller('MapController', function($scope, uiGmapGoogleMapApi) {

  $scope.map = {
    center: {
      latitude: 32.792447,
      longitude: -79.936134
    },
    zoom: 11,
  };
  $scope.marker = {
    coords: {
      latitude: 32.792447,
      longitude: -79.936134
    },
    id: 1,
    options: {
      icon:'/assets/images/website/marker.png'
    }
  };
  $scope.windowOptions = {
    visible: false
  };

  $scope.onClick = function() {
    $scope.windowOptions.visible = !$scope.windowOptions.visible;
  };
  $scope.closeClick = function() {
    $scope.windowOptions.visible = false;
  };

      var styleArray = [
    {
        'featureType': 'all',
        'elementType': 'labels.text.fill',
        'stylers': [
            {
                'saturation': 36
            },
            {
                'color': '#000000'
            },
            {
                'lightness': 40
            }
        ]
    },
    {
        'featureType': 'all',
        'elementType': 'labels.text.stroke',
        'stylers': [
            {
                'visibility': 'on'
            },
            {
                'color': '#000000'
            },
            {
                'lightness': 16
            }
        ]
    },
    {
        'featureType': 'all',
        'elementType': 'labels.icon',
        'stylers': [
            {
                'visibility': 'off'
            }
        ]
    },
    {
        'featureType': 'administrative',
        'elementType': 'geometry.fill',
        'stylers': [
            {
                'color': '#000000'
            },
            {
                'lightness': 20
            }
        ]
    },
    {
        'featureType': 'administrative',
        'elementType': 'geometry.stroke',
        'stylers': [
            {
                'color': '#000000'
            },
            {
                'lightness': 17
            },
            {
                'weight': 1.2
            }
        ]
    },
    {
        'featureType': 'landscape',
        'elementType': 'geometry',
        'stylers': [
            {
                'color': '#000000'
            },
            {
                'lightness': 20
            }
        ]
    },
    {
        'featureType': 'poi',
        'elementType': 'geometry',
        'stylers': [
            {
                'color': '#000000'
            },
            {
                'lightness': 21
            }
        ]
    },
    {
        'featureType': 'road.highway',
        'elementType': 'geometry.fill',
        'stylers': [
            {
                'color': '#000000'
            },
            {
                'lightness': 17
            }
        ]
    },
    {
        'featureType': 'road.highway',
        'elementType': 'geometry.stroke',
        'stylers': [
            {
                'color': '#000000'
            },
            {
                'lightness': 29
            },
            {
                'weight': 0.2
            }
        ]
    },
    {
        'featureType': 'road.arterial',
        'elementType': 'geometry',
        'stylers': [
            {
                'color': '#000000'
            },
            {
                'lightness': 18
            }
        ]
    },
    {
        'featureType': 'road.local',
        'elementType': 'geometry',
        'stylers': [
            {
                'color': '#000000'
            },
            {
                'lightness': 16
            }
        ]
    },
    {
        'featureType': 'transit',
        'elementType': 'geometry',
        'stylers': [
            {
                'color': '#000000'
            },
            {
                'lightness': 19
            }
        ]
    },
    {
        'featureType': 'water',
        'elementType': 'geometry',
        "stylers": [
            {
                'color': '#000000'
            },
            {
                'lightness': 17
            }
        ]
    }
];

$scope.options = {
   scrollwheel: false,
   maxZoom: 11,
   minZoom: 11,
   disableDefaultUI: true,
   styles: styleArray
};

      uiGmapGoogleMapApi.then(function(maps) {

      });
  });

})();
