(function(){
  'use strict';
  angular
    .module('glasser', [
      'ngMaterial',
      'ngMdIcons',
      'chart.js',
      'uiGmapgoogle-maps',
      'duScroll'
    ])

    .value('duScrollDuration', 2000)
    .value('duScrollOffset', 0)
    .config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
        key: 'AIzaSyBjn_JnmB5-anlcP2jenrS702gZQ-ck7y8',
        v: '3.20', //defaults to latest 3.X anyhow
        libraries: 'weather,geometry,visualization'
    });
})

})();
